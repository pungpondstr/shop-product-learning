@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6" align='left'>
                            <h1>Edit</h1>
                        </div>
                    </div>
                </div>

                <form action="{{url("/product/$products->id")}}" method="post">
                    {{ csrf_field() }}
                    @method('PUT')
                    <div class="card-body">
                        <label>ProductName</label>
                        <input class="form-control mb-2" type="text" name="productName" value="{{$products->productName}}" required>
    
                        <label>Picture</label>
                        <input class="form-control mb-2" type="text" name="picture" value="{{$products->picture}}" required>
    
                        <label>Category</label>
                        <input class="form-control mb-2" type="text" name="category" value="{{$products->category}}" required>
    
                        <label>Product Description</label>
                        <input class="form-control mb-2" type="text" name="productDescription" value="{{$products->productDescription}}" required>
    
                        <label>Price</label>
                        <input class="form-control mb-2" type="number" name="price" value="{{$products->price}}" required>
    
                        <label>Quantity Stock</label>
                        <input class="form-control mb-2" type="number" name="quantityStock" value="{{$products->quantityStock}}" required>
    
                        <div align='right'>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
