@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6" align='left'>
                            <h1>View</h1>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <img src="{{$products->picture}}" style="width: 100%; height: 300px; object-fit: cover; margin-bottom: 20px;">
                    <label>ProductName</label>
                    <input class="form-control mb-2" type="text" readonly value="{{$products->productName}}">

                    <label>Category</label>
                    <input class="form-control mb-2" type="text" readonly value="{{$products->category}}">

                    <label>Product Description</label>
                    <input class="form-control mb-2" type="text" readonly value="{{$products->productDescription}}">

                    <label>Price</label>
                    <input class="form-control mb-2" type="number" readonly value="{{$products->price}}">

                    <label>Quantity Stock</label>
                    <input class="form-control mb-2" type="number" readonly value="{{$products->quantityStock}}">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
