@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6" align='left'>
                                <h1>Products</h1>
                            </div>

                            <div class="col-6" align='right'>
                                <a href="/product/create" class="btn btn-primary">Create a new product</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <input class="form-control" type="text" placeholder="Search:" onkeyup="myFunction()"
                                    id="myInput">
                            </div>
                            <div class="col-4">
                                <button class="btn btn-outline-info" onclick="clearData()">Reset</button>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-bordered" id="myTable">
                            <tr align='center'>
                                <th>รหัสสินค้า</th>
                                <th>ชื่อสินค้า</th>
                                <th>รูปภาพสินค้า</th>
                                <th>ประเภทสินค้า</th>
                                <th>รายละเอียดสินค้า</th>
                                <th>ราคาสินค้า</th>
                                <th>จำนวนสินค้า</th>
                                <th>Action</th>
                            </tr>

                            @foreach ($products as $item)
                                <tr align='center'>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->productName }}</td>
                                    <td>
                                        <img src="{{ $item->picture }}" alt="" width="50" height="50">
                                    </td>
                                    <td>{{ $item->category }}</td>
                                    <td>{{ $item->productDescription }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>{{ $item->quantityStock }}</td>

                                    <td>
                                        <form action="{{ url("/product/$item->id") }}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            <a class="btn btn-info" href="{{ url("/product/$item->id") }}">View</a>
                                            <a class="btn btn-warning"
                                                href="{{ url("/product/$item->id/edit") }}">Edit</a>
                                            <button class="btn btn-danger" onclick="delete_object()">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function delete_object() {
            if (!confirm("คุณแน่ใจแล้วใช่ไหมที่จะลบ ?"))
                event.preventDefault();
        }

        // function myFunction() {
        //     var input, filter, table, tr, td, i, txtValue;
        //     input = document.getElementById("myInput");
        //     filter = input.value.toUpperCase();
        //     table = document.getElementById("myTable");
        //     tr = table.getElementsByTagName("tr");
        //     for (i = 0; i < tr.length; i++) {
        //         td1 = tr[i].getElementsByTagName("td")[0];
        //         td2 = tr[i].getElementsByTagName("td")[1];
        //         td3 = tr[i].getElementsByTagName("td")[2];
        //         td4 = tr[i].getElementsByTagName("td")[3];
        //         td5 = tr[i].getElementsByTagName("td")[4];
        //         td6 = tr[i].getElementsByTagName("td")[5];
        //         td7 = tr[i].getElementsByTagName("td")[6];
        //         if (td1) {
        //             txtValue = td1.textContent || td1.innerText;
        //             if (txtValue.toUpperCase().indexOf(filter) > -1) {
        //                 tr[i].style.display = "";
        //             } else {
        //                 tr[i].style.display = "none";
        //             }
        //         }


        //     }
        // }

        function myFunction() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0]; // for column one
                td1 = tr[i].getElementsByTagName("td")[1]; // for column two
                td2 = tr[i].getElementsByTagName("td")[2];
                td3 = tr[i].getElementsByTagName("td")[3];
                td4 = tr[i].getElementsByTagName("td")[4];
                td5 = tr[i].getElementsByTagName("td")[5];
                td6 = tr[i].getElementsByTagName("td")[6];
                /* ADD columns here that you want you to filter to be used on */
                if (td) {
                    if ((td.innerHTML.toUpperCase().indexOf(filter) > -1) || (td1.innerHTML.toUpperCase().indexOf(filter) >
                            -1) || (td2.innerHTML.toUpperCase().indexOf(filter) >
                            -1) || (td3.innerHTML.toUpperCase().indexOf(filter) >
                            -1) || (td4.innerHTML.toUpperCase().indexOf(filter) >
                            -1) || (td5.innerHTML.toUpperCase().indexOf(filter) >
                            -1) || (td6.innerHTML.toUpperCase().indexOf(filter) >
                            -1)) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }

        function clearData() {
            console.log(document.getElementById('myInput').value);
            document.getElementById('myInput').value = '';
            myFunction();
        }

    </script>

@endsection
