<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
        'productName',
        'picture',
        'category',
        'productDescription',
        'price',
        'quantityStock',
    ];
}
